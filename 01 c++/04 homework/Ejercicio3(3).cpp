/**
 * @file Template.cpp
 * @author Juan Carlos Pineda Gonzales (juancarlospinedagonzales363@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
    int NumberDisapproved = 0;
	int NumberApproved  = 0;
	int NumberNotable = 0;
	int NumberOutstanding = 0;
	
	float PorcentagePastMatter = 0.0;
	float PorcentageDisapproved = 0.0;
	float PorcentageApproved = 0.0;
	float PorcentageNotable = 0.0;
	float PorcentageOutstanding = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResult();
float CalculatePorcentagePastMatter(int Disapproved, int Approved, int Notable, int Outstanding);
float CalculatePorcentageDisapproved(int Disapproved, int Approved, int Notable, int Outstanding);
float CalculatePorcentageApproved(int Disapproved, int Approved, int Notable, int Outstanding);
float CalculatePorcentageNotable(int Disapproved, int Approved, int Notable, int Outstanding);
float CalculatePorcentageOutstanding(int Disapproved, int Approved, int Notable, int Outstanding3);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
 CollectData();
 Calculate();
 ShowResult();
	
}
//=====================================================================================================

void CollectData(){
	cout<<"escribir el numero de alumnos desaprovados ";
	cin>>NumberDisapproved;
	cout<<"escribir el numero de alumnos aprovados ";
	cin>>NumberApproved;
	cout<<"escribir el numero de alumnos notables  ";
	cin>>NumberNotable;
	cout<<"escribir el numero de alumnos sobresaliente ";
	cin>>NumberOutstanding;
	
}
//=====================================================================================================

void Calculate(){
	PorcentagePastMatter = CalculatePorcentagePastMatter(NumberDisapproved, NumberApproved, NumberNotable, NumberOutstanding);
	PorcentageDisapproved = CalculatePorcentageDisapproved(NumberDisapproved, NumberApproved, NumberNotable, NumberOutstanding);
	PorcentageApproved = CalculatePorcentageApproved(NumberDisapproved, NumberApproved, NumberNotable, NumberOutstanding);
	PorcentageNotable = CalculatePorcentageNotable(NumberDisapproved, NumberApproved, NumberNotable, NumberOutstanding);
	PorcentageOutstanding = CalculatePorcentageOutstanding(NumberDisapproved, NumberApproved, NumberNotable, NumberOutstanding);
}
//=====================================================================================================

void ShowResult(){
	cout<<"el porcentaje de alumnos que superaron la asignatura es :"<<PorcentagePastMatter;
	cout<<"\n\rel porcentaje de alumnos que desaprobaron es :"<<PorcentageDisapproved;
	cout<<"\n\rel porcentaje de alumnos que aprovaron es :"<<PorcentageApproved;
	cout<<"\n\relprocentaje de alumnos notables son :"<<PorcentageNotable;
	cout<<"\n\rel porcentaje de alumnnos sobresalientes son :"<<PorcentageOutstanding;
}
//=====================================================================================================
 
 float CalculatePorcentagePastMatter(int Disapproved, int Approved, int Notable, int Outstanding){
 	return ((float)Approved + Notable + Outstanding)*100/(Disapproved+Approved+Notable+Outstanding);
 }
//=====================================================================================================

float CalculatePorcentageDisapproved(int Disapproved, int Approved, int Notable, int Outstanding){
	return (float)Disapproved*100/(Disapproved+Approved+Notable+Outstanding);
}
//=====================================================================================================

float CalculatePorcentageApproved(int Disapproved, int Approved, int Notable, int Outstanding){
	return (float)Approved*100/(Disapproved+Approved+Notable+Outstanding);
}
//=====================================================================================================

float CalculatePorcentageNotable(int Disapproved, int Approved, int Notable, int Outstanding){
	return (float)Notable*100/(Disapproved+Approved+Notable+Outstanding);
}
//=====================================================================================================

float CalculatePorcentageOutstanding(int Disapproved, int Approved, int Notable, int Outstanding){
	return (float)Outstanding*100/(Disapproved+Approved+Notable+Outstanding);
}
