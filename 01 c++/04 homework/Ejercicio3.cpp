#include <iostream>

using namespace std;

int main(){
	 
	//Declaration of exercise 03
	int NumberDisapproved;
	int NumberApproved;
	int NumberNotable;
	int NumberOutstanding;
	int TotalNotes;
	float PorcentagePastMatter;
	float PorcentageDisapproved;
	float PorcentageApproved;
	float PorcentageNotable;
	float PorcentageOutstanding;
	
	//Initialize
	NumberDisapproved = 0;
	NumberApproved = 0;
	NumberNotable = 0;
	NumberOutstanding = 0;
	TotalNotes = 0;
	PorcentagePastMatter = 0;
	PorcentageDisapproved = 0;
	PorcentageApproved = 0;
    PorcentageNotable = 0;
	PorcentageOutstanding = 0;
	
	//Display phrase 01
	cout<<"escribir el numero de alumnos desaprovados ";
	cin>>NumberDisapproved;
	cout<<"escribir el numero de alumnos aprovados ";
	cin>>NumberApproved;
	cout<<"escribir el numero de alumnos notables  ";
	cin>>NumberNotable;
	cout<<"escribir el numero de alumnos sobresaliente ";
	cin>>NumberOutstanding;
	
	//Porcentage disapproved, approved, notable, outstanding, past matter
	TotalNotes = NumberDisapproved + NumberApproved + NumberNotable + NumberOutstanding;
	PorcentagePastMatter = (NumberApproved + NumberNotable + NumberOutstanding)*100/TotalNotes;
	PorcentageDisapproved = NumberDisapproved*100/TotalNotes;
	PorcentageApproved = NumberApproved*100/TotalNotes;
	PorcentageNotable = NumberNotable*100/TotalNotes;
	PorcentageOutstanding = NumberOutstanding*100/TotalNotes;
	
	cout<<"el porcentaje de alumnos que superaron la asignatura es :"<<PorcentagePastMatter;
	cout<<"\n\rel porcentaje de alumnos que desaprobaron es :"<<PorcentageDisapproved;
	cout<<"\n\rel porcentaje de alumnos que aprovaron es :"<<PorcentageApproved;
	cout<<"\n\relprocentaje de alumnos notables son :"<<PorcentageNotable;
	cout<<"\n\rel porcentaje de alumnnos sobresalientes son :"<<PorcentageOutstanding;
	
	return 0;
	
}
