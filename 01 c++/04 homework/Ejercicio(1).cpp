/**
 * @file Template.cpp
 * @author Juan Carlos Pineda Gonzales (juancarlospinedagonales363@gmail.com)
 * @brief calcular, el �rea lateral y el volumen del cilindro
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416      // Constante Pi

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float  terminalInputHeight = 0.0;
float  terminalInputRadius = 0.0;

float circularArea = 0.0;
float lateralAreaCylinder = 0.0;
float volumenCylinder = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateCircularArea(float radius);
float CalculateLateralAreaCylinder(float radius, float height);
float CalculateVolumeCylinder(float radius, float height);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"Escribir el radio del cilindro: ";
	cin>>terminalInputRadius;
	cout<<"Escribir la altura del cilindro: ";
	cin>>terminalInputHeight;
}
//=====================================================================================================

void Calculate(){
	circularArea = CalculateCircularArea(terminalInputRadius);
	lateralAreaCylinder = CalculateLateralAreaCylinder(terminalInputHeight, terminalInputRadius);
	volumenCylinder = CalculateVolumeCylinder(terminalInputHeight, terminalInputRadius); 
}
//=====================================================================================================

void ShowResults(){
	cout<<"\tEl area del circulo es: "<< circularArea<<"\r\n";
	cout<<"\tEl area lateral es: "<<lateralAreaCylinder<<"\r\n";
	cout<<"\tEl  volumen es: "<<volumenCylinder<<"\r\n";	
}
//=====================================================================================================

float CalculateCircularArea(float radius){
	return PI*pow(radius,2.0);
}
//=====================================================================================================

float CalculateLateralAreaCylinder(float radius, float height){
	return 2.0*PI*radius*height;
}
//===================================================================================================

float CalculateVolumeCylinder(float radius, float height){
	return PI*pow(radius,2.0)*height;
}
