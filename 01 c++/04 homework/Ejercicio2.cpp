/**
 * @file Template.cpp
 * @author Juan Carlos Pineda Gonzales (juancarlospinedagonzales363@gmail.com)
 * @brief Porcentaje de hombres y que porcentaje de mujeres hay en un grupo de estudiantes. 
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
    int NumberBoys = 0;                   //Entrada del numero de chicos a traves del terminal
	int NumberGirls =0;                   //Entrada del numero de chicas a traves del terminal
    
	float PorcentageBoys = 0.0;           //Porcentaje del numero de chicos
	float PorcentageGirls = 0.0;          //Porcentaje del numero de chicas
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculatePorcentageBoys(int boys, int girls);
float CalculatePorcentageGirls(int boys, int girls);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
	
}
//=====================================================================================================

void CollectData(){
	cout<<"========Insert Data========\r\n";
	cout<<"escribir el numero de chicos ";
	cin>>NumberBoys;
	cout<<"escribir el numero de chicas ";
	cin>>NumberGirls;
	
}
//=====================================================================================================

void Calculate(){
	PorcentageBoys = CalculatePorcentageBoys(NumberBoys , NumberGirls);
	PorcentageGirls = CalculatePorcentageGirls(NumberBoys , NumberGirls);
}
//=====================================================================================================

void ShowResults(){
	cout<<"el porcentaje de chicos es : "<<PorcentageBoys;
	cout<<"\n\rel porcentaje de chicas es : "<<PorcentageGirls;
}
//=====================================================================================================

float CalculatePorcentageBoys(int boys, int girls){
	return ((float)boys*100/(boys+girls));
}
//=====================================================================================================

float CalculatePorcentageGirls(int boys, int girls){
	return ((float)girls*100/(boys+girls));
}
