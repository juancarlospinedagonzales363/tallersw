#include <iostream>

using namespace std;

int main(){
	
	//Declaration of exercise 04
	float TemperatureCelsius;
	float TemperatureFahrenheit;
	float QuantityInches;
	float QuantityMillimeters;
	
	//Initialize
	TemperatureCelsius = 0;
	TemperatureFahrenheit = 0;
	QuantityInches = 0;
	QuantityMillimeters = 0;
	
	//Display phrase 01
	cout<<"escribir temperatura en grado celsius ";
	cin>>TemperatureCelsius;
	cout<<"escribir cantidad de agua en pulgadas ";
	cin>>QuantityInches;
	
	//temperature and quantity conversions
	TemperatureFahrenheit = (TemperatureCelsius*9/5)+32;
	QuantityMillimeters = QuantityInches*25.5;
	
	cout<<"la tempera del agua en grado Fahrenheit es :"<<TemperatureFahrenheit;
	cout<<"\n\rla cantidad de agua en milimetros es :"<<QuantityMillimeters;
	
	
	return 0;
	
	
}
